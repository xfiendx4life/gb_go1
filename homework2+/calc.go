package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

// Structure for operation, where name is a string name for operation and priority is 1 for + and - and 2 for the rest
type operation struct {
	name      string
	priority  int
	lastIndex int
}

type operationMaker interface {
	resetLastIndex()
	findElementLastIndex(*[]string)
}

var _ operationMaker = &operation{}

var (
	mult              = operation{"*", 2, -1}
	div               = operation{"/", 2, -1}
	sum               = operation{"+", 1, -1}
	minus             = operation{"-", 1, -1}
	mod               = operation{"%", 2, -1}
	allowedOPerations = map[string]bool{
		mult.name:  true,
		div.name:   true,
		sum.name:   true,
		minus.name: true,
		mod.name:   true,
	}
)

type node struct {
	data  string
	left  *node
	right *node
}

func (o *operation) resetLastIndex() {
	o.lastIndex = -1
}

func resetIndexes() {
	sum.resetLastIndex()
	minus.resetLastIndex()
	mult.resetLastIndex()
	div.resetLastIndex()
	mod.resetLastIndex()
}

// max с приоритетами
func max(a ...operation) (m int) {
	m = -1
	priority := 2
	for _, item := range a {
		if item.lastIndex > m && item.priority <= priority && item.priority != -1 {
			m = item.lastIndex
			priority = item.priority
		}
	}
	return
}

func (op *operation) findElementLastIndex(m *[]string) {
	for i, item := range *m {
		if item == op.name {
			op.lastIndex = i
		}
	}
}

// подумать как разбить по приоритетам
func getLastEl(m []string) int {
	resetIndexes()
	sum.findElementLastIndex(&m)
	minus.findElementLastIndex(&m)
	mult.findElementLastIndex(&m)
	div.findElementLastIndex(&m)
	mod.findElementLastIndex(&m)
	return max(sum, minus, mult, div, mod)
}

func (root *node) parseArray(m []string) {
	if len(m) == 1 {
		root.data = m[0]
		return
	}
	ind := getLastEl(m)
	root.data = m[ind]
	var left node
	root.left = &left
	root.left.parseArray(m[:ind])
	var right node
	root.right = &right
	root.right.parseArray(m[ind+1:])

}

func (root *node) postorder(m *[]string) {
	if root != nil {
		root.left.postorder(m)
		root.right.postorder(m)
		*m = append(*m, root.data)
	}
}

func makeOperation(a, b float64, oper string) (result float64, err error) {
	switch oper {
	case sum.name:
		result = a + b
	case minus.name:
		result = a - b
	case div.name:
		if b == 0 {
			err = fmt.Errorf("деление на 0")
		} else {
			result = a / b
		}

	case mult.name:
		result = a * b
	case mod.name:
		result = math.Mod(a, b) // float64(int(a) % int(b))
	}
	return
}

func countResult(m *[]string) (float64, error) {
	var stack []float64
	for _, item := range *m {
		if val, err := strconv.ParseFloat(item, 64); err == nil {
			stack = append(stack, val)
		} else {
			a, b := stack[len(stack)-2], stack[len(stack)-1]
			stack = stack[:len(stack)-2]
			res, err := makeOperation(a, b, item)
			if err != nil {
				return 0, err
			}
			stack = append(stack, res)
		}
	}
	return stack[0], nil
}

func normalizeString(s string) string {
	for strings.Contains(s, "  ") {
		s = strings.Replace(s, "  ", " ", 1)
	}

	s = strings.TrimPrefix(s, " ")
	s = strings.TrimSuffix(s, " ")
	return s
}

func checkIfInt(a float64) bool {
	return a == float64(int(a))
}

func printResult(post *[]string) {
	res, err := countResult(post)
	if err != nil {
		log.Fatal(err)
	}
	if checkIfInt(res) {
		fmt.Printf("%d\n", int(res))
	} else {
		fmt.Printf("%.5f\n", res)
	}
}

func validateData(arr []string) error {
	for ind, item := range arr {
		if ind%2 == 0 {
			if _, err := strconv.ParseFloat(item, 64); err != nil {
				return fmt.Errorf("недопустимый тип операнда")
			}
		} else {
			if _, ok := allowedOPerations[item]; !ok {
				return fmt.Errorf("недопустимая операция")
			}
		}

	}
	return nil
}

func main() {
	fmt.Println("ВВедите выражение через пробел")
	var s string
	scanner := bufio.NewScanner(os.Stdin)
	if scanner.Scan() {
		s = scanner.Text()
	}
	s = normalizeString(s)
	m := strings.Split(s, " ")
	if err := validateData(m); err != nil {
		log.Fatal(err)
	}
	var root node
	root.parseArray(m)
	post := make([]string, 0)
	root.postorder(&post)
	printResult(&post)
}
