package hw5

func Fib(n int) int {
	switch n {
	case 0:
		return 0
	case 1:
		return 1
	}
	return Fib(n-1) + Fib(n-2)
}

func CachedFib(n int) int {
	cache := map[int]int{}
	var f func(n int) int
	f = func(n int) int {
		switch n {
		case 0:
			cache[0] = 0
		case 1:
			cache[1] = 1
		}
		if _, ok := cache[n]; !ok {
			cache[n] = f(n-1) + f(n-2)
		}

		return cache[n]
	}
	return f(n)
}
