package hw5

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Cases in fib_test.go
func TestFibTestify(t *testing.T) {
	assert := assert.New(t)
	for i, c := range cases {
		if i < 10 {
			res := Fib(c.input)
			assert.Equal(res, c.correct, fmt.Sprintf(">> Input: %d want: %d got: %d", c.input, c.correct, res))
		}

	}
}

func TestCachedFibTestify(t *testing.T) {
	assert := assert.New(t)
	for _, c := range cases {
		res := CachedFib(c.input)
		assert.Equal(res, c.correct, fmt.Sprintf(">> Input: %d want: %d got: %d", c.input, c.correct, res))

	}
}
