package hw5

import "testing"

type testCases struct {
	input   int
	correct int
}

var cases = []testCases{
	{input: 0, correct: 0},
	{input: 1, correct: 1},
	{input: 2, correct: 1},
	{input: 3, correct: 2},
	{input: 4, correct: 3},
	{input: 5, correct: 5},
	{input: 6, correct: 8},
	{input: 12, correct: 144},
	{input: 17, correct: 1597},
	{input: 20, correct: 6765},
	{input: 40, correct: 102334155},
	// {input: 49, correct: 7778742049},
}

func TestFib(t *testing.T) {
	for i, c := range cases {
		if i < 10 { // он по времени не посчитает 49
			if res := Fib(c.input); res != c.correct {
				t.Fatalf("Input: %d, Correct value is %d, your value is %d", c.input, c.correct, res)
			}
		}

	}
}

func TestCachedFib(t *testing.T) {
	for _, c := range cases {
		if res := CachedFib(c.input); res != c.correct {
			t.Fatalf("Input: %d, Correct value is %d, your value is %d", c.input, c.correct, res)
		}

	}
}

func BenchmarkFib40(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Fib(20)
	}
}

func BenchmarkCachedFib40(b *testing.B) {
	for i := 0; i < b.N; i++ {
		CachedFib(20)
	}
}
