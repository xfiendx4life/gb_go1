package hw2

import (
	"fmt"
	"math"
)

func Circle() {
	var s float64
	fmt.Print("Введите площадь круга S = ")
	fmt.Scanln(&s)
	r := math.Sqrt(s / math.Pi)
	fmt.Printf("Диаметр d = %v\n", 2*r)
	fmt.Printf("Длина окружности l = %v", 2*math.Pi*r)

}
