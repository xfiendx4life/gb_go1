package hw2

import "fmt"

func Trex() {
	var num int16
	fmt.Print("Введите трехзначное число n = ")
	fmt.Scanln(&num)
	fmt.Printf("Число единиц  - %v, число десятков - %v, числое сотен - %v", num%10, num/10%10, num/100)
}
