package main

import (
	"fmt"
	"math"
)

func isPrime(n int) bool {
	for i := 2; i <= int(math.Sqrt(float64(n))); i++ {
		if n%i == 0 {
			return false
		}
	}
	return n != 0 && n != 1
}

func main() {
	var n int
	fmt.Print("Введите число, до которого выводим простые числа N = ")
	fmt.Scanln(&n)
	for i := 0; i <= n; i++ {
		if isPrime(i) {
			fmt.Printf("%d ", i)
		}
	}
}
