package main

import (
	"fmt"

	"insertionsort"

	"hw1.com/hw1"
	"hw2.com/hw2"

	"hw5.com/hw5"
)

func main() {
	fmt.Println("Выберите задание для запуска 1, 2, 4, 5")
	var num int
	fmt.Scanln(&num)
	switch num {
	case 1:
		hw1.Hw1()
	case 2:
		fmt.Println("Площадь какой фигуры вы хотите найти? 1 - Круг, 2 - Прямоугольник")
		fmt.Println("Если вы хотите найти сумму цифр трехзначного числа, нажмите 3")
		var a int
		fmt.Scanln(&a)
		switch a {
		case 1:
			hw2.Circle()
		case 2:
			hw2.Rect()
		case 3:
			hw2.Trex()
		}
	case 4:
		insertionsort.DoSort()

	case 5:
		fmt.Println("Чтобы найти число фибоначчи медленно, выбирайте 1, быстро - выбирайте 2")
		var n int
		fmt.Scanln(&n)
		var a int
		fmt.Print("Введите номер числа фибоначчи n = ")
		fmt.Scanln(&a)
		switch n {
		case 1:
			fmt.Println(hw5.Fib(a))
		case 2:
			fmt.Println(hw5.CachedFib(a))
		}
	default:
		fmt.Println("Некорректный номер")
	}

}
