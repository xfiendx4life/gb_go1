package main

import (
	"conf_reader"
	"fmt"
	"log"
	"os"
)

func main() {
	path, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(path)
	path += "/conf_reader/config.json"
	c := conf_reader.ReadFromFile(path)
	fmt.Println(c)
}
