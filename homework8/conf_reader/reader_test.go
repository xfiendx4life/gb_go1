package conf_reader

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"os"
	"strconv"
	"testing"
)

// "PORT"
// "DBURL"
// "JAEGERURL"
// "SENTRYURL"
// "APPID"
// "APPKEY"
// "KAFKA"

var testCases = map[string]map[string]string{
	"valid": {"PORT": "8000",
		"DBURL":     "http://dbtesturl.com",
		"JAEGERURL": "http://jaegertesturl.com",
		"SENTRYURL": "http://sentryrtesturl.com",
		"APPKEY":    "testkey",
		"APPID":     "testid",
		"KAFKA":     "9000"},

	"notValidUrl": {"PORT": "8000",
		"DBURL":     "notValidUrl",
		"JAEGERURL": "notValidUrl",
		"SENTRYURL": "notValidUrl",
		"APPKEY":    "testkey",
		"APPID":     "testid",
		"KAFKA":     "9000"},
	"empty": {},
}

var defaults = map[string]string{
	"PORT":      "8080",
	"DBURL":     "postgres://db-user:db-password@petstore-db:5432/petstore?sslmode=disable",
	"JAEGERURL": "http://jaeger:16686",
	"SENTRYURL": "http://sentry:9000",
	"KAFKA":     "9090",
	"APPKEY":    "testKey",
	"APPID":     "testID",
}

func prepare(key string) {
	for k, v := range testCases[key] {
		os.Setenv(k, v)
	}
}

func clear() {
	os.Clearenv()
}
func checkValid(c *Config, t *testing.T) {
	if p, _ := strconv.Atoi(testCases["valid"]["PORT"]); *c.Port != p {
		t.Fatalf("Port = %d, but should be %s", *c.Port, testCases["valid"]["PORT"])
	}
	if *c.AppID != testCases["valid"]["APPID"] {
		t.Fatalf("AppID = %s, but should be %s", *c.AppID, defaults["APPID"])
	}
	if *c.AppKey != testCases["valid"]["APPKEY"] {
		t.Fatalf("AppKey = %s, but should be %s", *c.AppKey, testCases["valid"]["APPKEY"])
	}
	if urlT, _ := url.Parse(testCases["valid"]["DBURL"]); *c.Dburl != *urlT {
		t.Fatalf("DB_url = %#v, but should be %#v", *c.Dburl, urlT)
	}
	if urlT, _ := url.Parse(testCases["valid"]["JAEGERURL"]); *c.JaegerUrl != *urlT {
		t.Fatalf("Jaeger_url = %#v, but should be %#v", *c.JaegerUrl, urlT)
	}
	if urlT, _ := url.Parse(testCases["valid"]["SENTRYURL"]); *c.SentryUrl != *urlT {
		t.Fatalf("sENTRY_url = %#v, but should be %#v", *c.SentryUrl, urlT)
	}

}
func TestValid(t *testing.T) {
	prepare("valid")
	c := ReadEnv()
	checkValid(c, t)
	clear()
}

func checkNotValidUrl(c *Config, t *testing.T) {
	if urlT, _ := url.Parse(defaults["JAEGERURL"]); *c.JaegerUrl != *urlT {
		t.Fatalf("Jaeger_url = %#v, but should be %#v", *c.JaegerUrl, urlT)
	}
	if urlT, _ := url.Parse(defaults["DBURL"]); (*c.Dburl).Scheme != urlT.Scheme || (*c.Dburl).Host != urlT.Host {
		t.Fatalf("DB_url = %#v, but should be %#v", (*c.Dburl).Host, urlT.Host) // тут странно парсит, визуально они одинаковые
	}
	if urlT, _ := url.Parse(defaults["SENTRYURL"]); *c.SentryUrl != *urlT {
		t.Fatalf("sENTRY_url = %#v, but should be %#v", *c.SentryUrl, urlT)
	}
}

func TestNotValidUrl(t *testing.T) {
	prepare("notValidUrl")
	c := ReadEnv()
	checkNotValidUrl(c, t)
	clear()
}

func checkDefaults(c *Config, t *testing.T) {
	if p, _ := strconv.Atoi(defaults["PORT"]); *c.Port != p {
		t.Fatalf("Port = %d, but should be %s", *c.Port, defaults["PORT"])
	}
	if *c.AppID != defaults["APPID"] {
		t.Fatalf("AppID = %s, but should be %s", *c.AppID, defaults["APPID"])
	}
	if *c.AppKey != defaults["APPKEY"] {
		t.Fatalf("AppKey = %s, but should be %s", *c.AppKey, defaults["APPKEY"])
	}
	if urlT, _ := url.Parse(defaults["DBURL"]); (*c.Dburl).Host != urlT.Host || (*c.Dburl).Scheme != urlT.Scheme {
		t.Fatalf("DB_url = %#v, but should be %#v", *c.Dburl, urlT)
	}
	if urlT, _ := url.Parse(defaults["JAEGERURL"]); *c.JaegerUrl != *urlT {
		t.Fatalf("Jaeger_url = %#v, but should be %#v", *c.JaegerUrl, urlT)
	}
	if urlT, _ := url.Parse(defaults["SENTRYURL"]); *c.SentryUrl != *urlT {
		t.Fatalf("sENTRY_url = %#v, but should be %#v", *c.SentryUrl, urlT)
	}
}

func TestDefaults(t *testing.T) {
	c := ReadEnv()
	checkDefaults(c, t)
}

type testFileSruct struct {
	Port        int    `json:"port"`
	DbURL       string `json:"db_url"`
	JaegerURL   string `json:"jaeger_url"`
	SentryURL   string `json:"sentry_url"`
	KafkaBroker int    `json:"kafka_broker"`
	AppID       string `json:"app_id"`
	AppKey      string `json:"app_key"`
}

func newTestStruct(key string) *testFileSruct {
	t := testFileSruct{}
	t.Port, _ = strconv.Atoi(testCases[key]["PORT"])
	t.DbURL = testCases[key]["DBURL"]
	t.JaegerURL = testCases[key]["JAEGERURL"]
	t.SentryURL = testCases[key]["SENTRYURL"]
	t.KafkaBroker, _ = strconv.Atoi(testCases[key]["KAFKA"])
	t.AppID = testCases[key]["APPID"]
	t.AppKey = testCases[key]["APPKEY"]
	return &t

}

func prepareTestFile(key string) (string, error) {
	file, err := json.MarshalIndent(newTestStruct(key), "", " ")
	if err != nil {
		log.Fatalf("couldn't create file %v", err)
		return "", err
	}
	path, _ := os.Getwd()
	path += "/testJson.json"
	err = os.WriteFile(path, file, 0666)
	if err != nil {
		log.Fatalf("couldn't create file %v", err)
		return "", err
	}
	return path, err
}

func deleteTestFile(path string) {
	err := os.Remove(path)
	if err != nil {
		log.Fatalf("couldn't remove test file %v", err)
	}
}

func TestFileValid(t *testing.T) {
	path, err := prepareTestFile("valid")
	if err != nil {
		t.Fatalf("couldn't create test file %v", err)
	}
	defer deleteTestFile(path)
	c := ReadFromFile(path)
	checkValid(c, t)

}

func TestFileNotValidUrl(t *testing.T) {
	path, err := prepareTestFile("notValidUrl")
	if err != nil {
		t.Fatalf("couldn't create test file %v", err)
	}
	defer deleteTestFile(path)
	c := ReadFromFile(path)
	checkNotValidUrl(c, t)
}

func TestFileDefaults(t *testing.T) {
	path, err := prepareTestFile("empty")
	if err != nil {
		t.Fatalf("couldn't create test file %v", err)
	}
	defer deleteTestFile(path)
	c := ReadFromFile(path)
	checkDefaults(c, t)

}

func ExampleReadFromFile() {
	path, err := prepareTestFile("valid")
	if err != nil {
		log.Fatalf("couldn't create test file %v", err)
	}
	defer deleteTestFile(path)
	c := ReadFromFile(path)
	fmt.Println(c)

	// Output:
	//  port: 8000
	//  db_url: http://dbtesturl.com
	//  jaeger_url: http://jaegertesturl.com
	//  sentry_url: http://sentryrtesturl.com
	//  kafka_broker: kafka:9000
	//  app_id: testid
	//  app_key: testkey

}
