package insertionsort

import (
	"math/rand"
	"sort"
	"testing"
	"time"
)

func createTestArray() []int {
	rand.Seed(time.Now().Unix())
	length := rand.Intn(10)
	a := make([]int, length)
	for i := 0; i < length; i++ {
		a[i] = rand.Intn(1000)
	}
	return a
}

func compareSlices(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i, itema := range a {
		if itema != b[i] {
			return false
		}
	}
	return true
}

func createReferenceArr(a []int) []int {
	b := make([]int, len(a))
	copy(b, a)
	return b
}

func sortReference(b []int) {
	sort.Slice(b, func(i, j int) bool {
		return b[i] < b[j]
	})
}

func doSorts() ([]int, []int) {
	a := createTestArray()
	a = insertionSort(a)
	b := createReferenceArr(a)
	sortReference(b)
	return a, b
}

func TestRandom(t *testing.T) {
	a, b := doSorts()
	t.Log(a)
	t.Log(b)
	if !compareSlices(a, b) {
		t.Fatal("not the same")
	}
}

func TestEmpty(t *testing.T) {
	a := make([]int, 0)
	b := createReferenceArr(a)
	a = insertionSort(a)
	sortReference(b)
	t.Log(a)
	t.Log(b)
	if !compareSlices(a, b) {
		t.Fatal("not the same")
	}
}

func TestOne(t *testing.T) {
	a := []int{1}
	b := createReferenceArr(a)
	a = insertionSort(a)
	sortReference(b)
	t.Log(a)
	t.Log(b)
	if !compareSlices(a, b) {
		t.Fatal("not the same")
	}
}

func TestSorted(t *testing.T) {
	a := []int{-4, 0, 12, 18, 20}
	b := createReferenceArr(a)
	a = insertionSort(a)
	sortReference(b)
	t.Log(a)
	t.Log(b)
	if !compareSlices(a, b) {
		t.Fatal("not the same")
	}
}

func TestReversed(t *testing.T) {
	a := []int{20, 18, 12, 0, -4}
	b := createReferenceArr(a)
	a = insertionSort(a)
	sortReference(b)
	t.Log(a)
	t.Log(b)
	if !compareSlices(a, b) {
		t.Fatal("not the same")
	}
}

func TestSameNumbers(t *testing.T) {
	a := []int{1, 1, 1, 1}
	b := createReferenceArr(a)
	a = insertionSort(a)
	sortReference(b)
	t.Log(a)
	t.Log(b)
	if !compareSlices(a, b) {
		t.Fatal("not the same")
	}
}
