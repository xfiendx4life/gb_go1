module main

go 1.16

replace hw1.com/hw1 => ./hw1

replace hw2.com/hw2 => ./homework2


replace insertionsort => ./homework4


replace hw5.com/hw5 => ./homework5

require hw1.com/hw1 v1.0.0

require hw2.com/hw2 v1.0.0

require hw5.com/hw5 v1.0.0

require insertionsort v0.0.0-00010101000000-000000000000
